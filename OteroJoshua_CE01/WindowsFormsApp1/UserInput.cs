﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class UserInput : Form
    {
        // Event Handler to send info to form 1
        public event EventHandler<TeamEventArg> SendInfo;
        public UserInput()
        {
            InitializeComponent();
        }

        public void LoadInfo(object sender, TeamEventArg e)
        {
            // Loads data from form 1 and fills in the input fields on the current user input window
            txtTeam.Text = e.TeamName;
            if (e.West == true)
            {
                rdoWest.Checked = true;
            }
            else
            {
                rdoEast.Checked = true;
            }
            
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            // Adds input info to eventarg and sends back to form 1
            TeamEventArg arg = new TeamEventArg();
            arg.TeamName = txtTeam.Text;
            arg.West = rdoWest.Checked;
            SendInfo?.Invoke(this, arg);
            // Closes user input window
            UserInput.ActiveForm.Close();
        }
    }
}
