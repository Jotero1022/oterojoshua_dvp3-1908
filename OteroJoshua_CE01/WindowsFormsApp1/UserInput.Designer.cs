﻿namespace WindowsFormsApp1
{
    partial class UserInput
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpBox = new System.Windows.Forms.GroupBox();
            this.lblTeam = new System.Windows.Forms.Label();
            this.txtTeam = new System.Windows.Forms.TextBox();
            this.rdoWest = new System.Windows.Forms.RadioButton();
            this.rdoEast = new System.Windows.Forms.RadioButton();
            this.btnAdd = new System.Windows.Forms.Button();
            this.grpBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpBox
            // 
            this.grpBox.Controls.Add(this.btnAdd);
            this.grpBox.Controls.Add(this.rdoEast);
            this.grpBox.Controls.Add(this.rdoWest);
            this.grpBox.Controls.Add(this.txtTeam);
            this.grpBox.Controls.Add(this.lblTeam);
            this.grpBox.Location = new System.Drawing.Point(12, 12);
            this.grpBox.Name = "grpBox";
            this.grpBox.Size = new System.Drawing.Size(337, 198);
            this.grpBox.TabIndex = 0;
            this.grpBox.TabStop = false;
            this.grpBox.Text = "Team Information";
            // 
            // lblTeam
            // 
            this.lblTeam.AutoSize = true;
            this.lblTeam.Location = new System.Drawing.Point(48, 59);
            this.lblTeam.Name = "lblTeam";
            this.lblTeam.Size = new System.Drawing.Size(68, 13);
            this.lblTeam.TabIndex = 0;
            this.lblTeam.Text = "Team Name:";
            // 
            // txtTeam
            // 
            this.txtTeam.Location = new System.Drawing.Point(122, 56);
            this.txtTeam.Name = "txtTeam";
            this.txtTeam.Size = new System.Drawing.Size(160, 20);
            this.txtTeam.TabIndex = 1;
            // 
            // rdoWest
            // 
            this.rdoWest.AutoSize = true;
            this.rdoWest.Checked = true;
            this.rdoWest.Location = new System.Drawing.Point(77, 121);
            this.rdoWest.Name = "rdoWest";
            this.rdoWest.Size = new System.Drawing.Size(50, 17);
            this.rdoWest.TabIndex = 2;
            this.rdoWest.TabStop = true;
            this.rdoWest.Text = "West";
            this.rdoWest.UseVisualStyleBackColor = true;
            // 
            // rdoEast
            // 
            this.rdoEast.AutoSize = true;
            this.rdoEast.Location = new System.Drawing.Point(187, 121);
            this.rdoEast.Name = "rdoEast";
            this.rdoEast.Size = new System.Drawing.Size(46, 17);
            this.rdoEast.TabIndex = 3;
            this.rdoEast.TabStop = true;
            this.rdoEast.Text = "East";
            this.rdoEast.UseVisualStyleBackColor = true;
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(122, 161);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 4;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // UserInput
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(361, 219);
            this.Controls.Add(this.grpBox);
            this.Name = "UserInput";
            this.Text = "UserInput";
            this.grpBox.ResumeLayout(false);
            this.grpBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpBox;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.RadioButton rdoEast;
        private System.Windows.Forms.RadioButton rdoWest;
        private System.Windows.Forms.TextBox txtTeam;
        private System.Windows.Forms.Label lblTeam;
    }
}