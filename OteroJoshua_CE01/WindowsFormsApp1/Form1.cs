﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        // Event Handler to send information to user input window
        public event EventHandler<TeamEventArg> SendInfo;
        // Stores teams when loaded from an XML file
        List<Teams> LoadedTeams = new List<Teams>();
        public Form1()
        {
            InitializeComponent();
        }

        void HandleClientWindowSize()
        {
            //Modify ONLY these float values
            float HeightValueToChange = 1.4f;
            float WidthValueToChange = 6.0f;

            //DO NOT MODIFY THIS CODE
            int height = Convert.ToInt32(Screen.PrimaryScreen.WorkingArea.Size.Height / HeightValueToChange);
            int width = Convert.ToInt32(Screen.PrimaryScreen.WorkingArea.Size.Width / WidthValueToChange);
            if (height < Size.Height)
                height = Size.Height;
            if (width < Size.Width)
                width = Size.Width;
            this.Size = new Size(width, height);
            //this.Size = new Size(376, 720);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            // create a userinput window and assign event handlers, then show the new window
            UserInput newWindow = new UserInput();
            newWindow.SendInfo += LoadData;
            newWindow.ShowDialog();
        }

        public void LoadData(object sender, TeamEventArg e)
        {
            // Create class to take data from Userinput window from the TeamEventArg
            Teams newItem = new Teams();
            newItem.TeamName = e.TeamName;
            newItem.West = e.West;
            // Send data to list box depending on if West or East was selected
            if (newItem.West == true)
            {
                lstWestern.Items.Add(newItem);
            }
            else
            {
                lstEastern.Items.Add(newItem);
            }

        }

        private void lstWestern_DoubleClick(object sender, EventArgs e)
        {
            // Checks if an item is selected before opening a new input window and filling the window with the information from the selected item
            if (lstWestern.SelectedItem != null)
            {
                Teams currentTeam = (Teams)lstWestern.SelectedItem;
                UserInput newWindow = new UserInput();
                newWindow.SendInfo += LoadData;
                TeamEventArg arg = new TeamEventArg();
                arg.TeamName = currentTeam.TeamName;
                arg.West = currentTeam.West;
                SendInfo += newWindow.LoadInfo;
                SendInfo?.Invoke(this, arg);
                lstWestern.Items.Remove(lstWestern.SelectedItem);
                newWindow.ShowDialog();
            }
        }

        private void lstEastern_DoubleClick(object sender, EventArgs e)
        {
            // Checks if an item is selected before opening a new input window and filling the window with the information from the selected item
            if (lstEastern.SelectedItem != null)
            {
                Teams currentTeam = (Teams)lstEastern.SelectedItem;
                UserInput newWindow = new UserInput();
                newWindow.SendInfo += LoadData;
                TeamEventArg arg = new TeamEventArg();
                arg.TeamName = currentTeam.TeamName;
                arg.West = currentTeam.West;
                SendInfo += newWindow.LoadInfo;
                SendInfo?.Invoke(this, arg);
                lstEastern.Items.Remove(lstEastern.SelectedItem);
                newWindow.ShowDialog();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            // Checks if an object is selected in each list box and deletes them
            if (lstWestern.SelectedItem != null)
            {
                lstWestern.Items.Remove(lstWestern.SelectedItem);
            }
            if (lstEastern.SelectedItem != null)
            {
                lstEastern.Items.Remove(lstEastern.SelectedItem);
            }
        }

        private void btnEast_Click(object sender, EventArgs e)
        {
            // sends object from the Western Team list box to the Eastern Team list box
            if (lstWestern.SelectedItem != null)
            {
                Teams currentTeam = (Teams)lstWestern.SelectedItem;
                currentTeam.West = false;
                lstWestern.Items.Remove(lstWestern.SelectedItem);
                lstEastern.Items.Add(currentTeam);
            }
        }

        private void btnWest_Click(object sender, EventArgs e)
        {
            // sends an object from the Eastern Team list box to the Western Team list box
            if (lstEastern.SelectedItem != null)
            {
                Teams currentTeam = (Teams)lstEastern.SelectedItem;
                currentTeam.West = true;
                lstEastern.Items.Remove(lstEastern.SelectedItem);
                lstWestern.Items.Add(currentTeam);
            }
        }

        private void saveListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // a new instance of the Save dialog
            SaveFileDialog dlg = new SaveFileDialog();

            // set the default extension
            dlg.DefaultExt = "xml";

            // if the user clicks the OK button
            if (DialogResult.OK == dlg.ShowDialog())
            {
                // we'll create a new instance of the XmlWriterSettings
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.ConformanceLevel = ConformanceLevel.Document;

                // we'll also set the indentation of the XML
                settings.Indent = true;

                // instantiate the XmlWriter
                using (XmlWriter writer = XmlWriter.Create(dlg.FileName, settings))
                {
                    // the first element we want will define what the data is
                    writer.WriteStartElement("TeamListData");
                    for (int i = 0; i < lstEastern.Items.Count; i++)
                    {
                        Teams newItem = (Teams)lstEastern.Items[i];
                        writer.WriteStartElement("EasternTeams");
                        writer.WriteElementString("Team", newItem.TeamName);
                        writer.WriteElementString("West", newItem.West.ToString());
                        writer.WriteEndElement();
                    }
                    for (int i = 0; i < lstWestern.Items.Count; i++)
                    {
                        Teams newItem = (Teams)lstWestern.Items[i];
                        writer.WriteStartElement("WesternTeams");
                        writer.WriteElementString("Team", newItem.TeamName);
                        writer.WriteElementString("West", newItem.West.ToString());
                        writer.WriteEndElement();
                    }
                    // close the initial element
                    writer.WriteEndElement();
                }
            }
        }

        private void loadListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // create the dialog to get the file name
            OpenFileDialog dlg = new OpenFileDialog();

            if (DialogResult.OK == dlg.ShowDialog())
            {
                lstEastern.Items.Clear();
                lstWestern.Items.Clear();
                LoadedTeams.Clear();
                XmlReaderSettings settings = new XmlReaderSettings();
                settings.ConformanceLevel = ConformanceLevel.Document;

                settings.IgnoreComments = true;
                settings.IgnoreWhitespace = true;

                // using the XmlReader
                using (XmlReader reader = XmlReader.Create(dlg.FileName, settings))
                {
                    // skip the metadata
                    reader.MoveToContent();

                    // verify that this is our team data
                    if (reader.Name != "TeamListData")
                    {
                        // return that this is not right
                        MessageBox.Show("This is not a file created from this program.");
                        return;
                    }

                    // if it is, move through the data and get what we want
                    while (reader.Read())
                    {
                        Teams newTeam = new Teams();
                        // Reads data and saves the team to a list of teams
                        if (reader.ReadToFollowing("Team"))
                        {
                            newTeam.TeamName = reader.ReadElementContentAsString();

                        }
                        if (reader.Name == "West" && reader.IsStartElement())
                        {
                            string boolString;
                            boolString = reader.ReadElementContentAsString();
                            newTeam.West = bool.Parse(boolString);
                            LoadedTeams.Add(newTeam);
                        }
                        newTeam = new Teams();
                        if (reader.ReadToFollowing("Team"))
                        {
                            newTeam.TeamName = reader.ReadElementContentAsString();
                        }
                        if (reader.Name == "West" && reader.IsStartElement())
                        {
                            string boolString;
                            boolString = reader.ReadElementContentAsString();
                            newTeam.West = bool.Parse(boolString);
                            LoadedTeams.Add(newTeam);
                        }

                    }
                }
                for (int i = 0; i < LoadedTeams.Count; i++)
                {
                    // Goes through the list of teams and assigns each team object to its correct list box
                    if (LoadedTeams[i].West == true)
                    {
                        lstWestern.Items.Add(LoadedTeams[i]);
                    }
                    else
                    {
                        lstEastern.Items.Add(LoadedTeams[i]);
                    }
                }
            }
        }

        private void printListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            printDialog1.ShowDialog();
        }
    }
}
