﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    class Teams
    {
        public string TeamName;

        public bool West;

        public override string ToString()
        {
            return $"{TeamName}";
        }
    }
}
