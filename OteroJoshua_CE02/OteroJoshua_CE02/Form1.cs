﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.IO;

namespace OteroJoshua_CE02
{
    public partial class Form1 : Form
    {
        // Set's up a MySqlConnection
        MySqlConnection conn = new MySqlConnection();
        // Set's up a Data Table to store Database information
        DataTable tableData = new DataTable();

        public Form1()
        {
            InitializeComponent();
            string connString = DBUtilities.BuildConnectionString();

            // Invoke the method to make the connect
            conn = DBUtilities.Connect(connString);

            // If RetrieveData returns false, automatically closes the application
            if (RetrieveData() == false)
            {
                Environment.Exit(1);
            }
            // Code to change the size of the Iphone image.
            HandleClientWindowSize();
        }

        void HandleClientWindowSize()
        {
            //Modify ONLY these float values
            float HeightValueToChange = 1.4f;
            float WidthValueToChange = 6.0f;

            //DO NOT MODIFY THIS CODE
            int height = Convert.ToInt32(Screen.PrimaryScreen.WorkingArea.Size.Height / HeightValueToChange);
            int width = Convert.ToInt32(Screen.PrimaryScreen.WorkingArea.Size.Width / WidthValueToChange);
            if (height < Size.Height)
                height = Size.Height;
            if (width < Size.Width)
                width = Size.Width;
            this.Size = new Size(width, height);
            //this.Size = new Size(376, 720);
        }

        // method to retrieve data from the database.
        private bool RetrieveData()
        {
            try
            {
                // create the SQL statement with a Limit of 10
                string sql = "SELECT CourseName, CourseNumber, Term, CreditHours, Track FROM classes LIMIT 10;";

                // Create the data adapter
                MySqlDataAdapter adr = new MySqlDataAdapter(sql, conn);

                // set the type for the SELECT command
                adr.SelectCommand.CommandType = CommandType.Text;


                // The Fill method adds rows to match the data source
                adr.Fill(tableData);

                // we can also get a count of the rows
                int numOfRows = tableData.Select().Length;

                // now we can fill in the form
                txtName.Text = tableData.Rows[0]["CourseName"].ToString();

                txtNumber.Text = tableData.Rows[0]["CourseNumber"].ToString();

                txtTerm.Text = tableData.Rows[0]["Term"].ToString();

                numHours.Text = tableData.Rows[0]["CreditHours"].ToString();

                cmbTrack.SelectedIndex = int.Parse(tableData.Rows[0]["Track"].ToString());
                // For Loop to enter all data from database into List View
                for (int i = 0; i < tableData.Select().Length; i++)
                {
                    MobileDev newClass = new MobileDev();
                    newClass.Name = tableData.Rows[i]["CourseName"].ToString();

                    newClass.Number = tableData.Rows[i]["CourseNumber"].ToString();

                    newClass.Term = tableData.Rows[i]["Term"].ToString();

                    newClass.CreditHours = decimal.Parse(tableData.Rows[i]["CreditHours"].ToString());

                    newClass.Track = int.Parse(tableData.Rows[i]["Track"].ToString());

                    // Create List View Item to populate List View
                    ListViewItem lvi = new ListViewItem();

                    lvi.Text = newClass.ToString();
                    // add the obnject to the Tag property
                    lvi.Tag = newClass;
                    // add the ImageIndex to the ListViewItem
                    lvi.ImageIndex = newClass.Track;
                    // add the ListViewItem to the ListView
                    lvClasses.Items.Add(lvi);
                }

                return true;
            }
            catch (MySqlException e)
            {
                // If there is no connection to the database the program will display the message below and close.
                MessageBox.Show("No Database Connection, Program will not run.");
                return false;
            }




        }

        private void lvClasses_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            // Creates a MobileDev object with data stored in the ListView object that the user double clicked.
            MobileDev newClass = (MobileDev)lvClasses.SelectedItems[0].Tag;
            // Then sends all the data recieved into the fields
            txtName.Text = newClass.Name;
            txtNumber.Text = newClass.Number;
            txtTerm.Text = newClass.Term;
            numHours.Value = newClass.CreditHours;
            cmbTrack.SelectedIndex = newClass.Track;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            MobileDev newClass = new MobileDev();
            // Then recieves all the data from the fields to insert into the database and the list view
            newClass.Name = txtName.Text;
            newClass.Number = txtNumber.Text;
            newClass.Term = txtTerm.Text;
            newClass.CreditHours = numHours.Value;
            newClass.Track = cmbTrack.SelectedIndex;
            try
            {
                // checks if a connection is open and then inserts new data into th database.
                if (conn.State == ConnectionState.Open)
                {
                    string stm = "INSERT INTO classes (CourseName, CourseNumber, Term, CreditHours, Track) VALUES (@name, @num, @term, @hours, @track)";

                    MySqlCommand cmd = new MySqlCommand(stm, conn);
                    cmd.Parameters.AddWithValue("@name", newClass.Name);
                    cmd.Parameters.AddWithValue("@num", newClass.Number);
                    cmd.Parameters.AddWithValue("@term", newClass.Term);
                    cmd.Parameters.AddWithValue("@hours", newClass.CreditHours);
                    cmd.Parameters.AddWithValue("@track", newClass.Track);

                    MySqlDataReader rdr = cmd.ExecuteReader();
                    rdr.Close();
                }
                else
                {
                    conn.Open();
                    string stm = "INSERT INTO classes (CourseName, CourseNumber, Term, CreditHours, Track) VALUES (@name, @num, @term, @hours, @track)";

                    MySqlCommand cmd = new MySqlCommand(stm, conn);
                    cmd.Parameters.AddWithValue("@name", newClass.Name);
                    cmd.Parameters.AddWithValue("@num", newClass.Number);
                    cmd.Parameters.AddWithValue("@term", newClass.Term);
                    cmd.Parameters.AddWithValue("@hours", newClass.CreditHours);
                    cmd.Parameters.AddWithValue("@track", newClass.Track);

                    MySqlDataReader rdr = cmd.ExecuteReader();
                    rdr.Close();
                    conn.Close();
                }
                // Updates the List View Afterwards
                ListViewItem lvi = new ListViewItem();

                lvi.Text = newClass.ToString();
                // add the object to the Tag property
                lvi.Tag = newClass;
                // add the ImageIndex to the ListViewItem, using the Track number.
                lvi.ImageIndex = newClass.Track;
                // add the ListViewItem to the ListView
                lvClasses.Items.Add(lvi);
            }
            catch (Exception er)
            {
                MessageBox.Show($"Add Failed\r\n{er.ToString()}");
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            MobileDev newClass = new MobileDev();
            // Then recieves all the data from the fields to use in the update process of both list view and database
            newClass.Name = txtName.Text;
            newClass.Number = txtNumber.Text;
            newClass.Term = txtTerm.Text;
            newClass.CreditHours = numHours.Value;
            newClass.Track = cmbTrack.SelectedIndex;
            try
            {
                // checks if a connection is open and then updates the database
                if (conn.State == ConnectionState.Open)
                {
                    string stm = "Update classes SET CourseName = @name, CourseNumber = @num, Term = @term, CreditHours = @hours, Track = @track WHERE CourseName = @name";

                    MySqlCommand cmd = new MySqlCommand(stm, conn);
                    cmd.Parameters.AddWithValue("@name", newClass.Name);
                    cmd.Parameters.AddWithValue("@num", newClass.Number);
                    cmd.Parameters.AddWithValue("@term", newClass.Term);
                    cmd.Parameters.AddWithValue("@hours", newClass.CreditHours);
                    cmd.Parameters.AddWithValue("@track", newClass.Track);

                    MySqlDataReader rdr = cmd.ExecuteReader();
                    rdr.Close();
                }
                else
                {
                    conn.Open();
                    string stm = "Update classes SET CourseName = @name, CourseNumber = @num, Term = @term, CreditHours = @hours, Track = @track WHERE CourseName = @name";

                    MySqlCommand cmd = new MySqlCommand(stm, conn);
                    cmd.Parameters.AddWithValue("@name", newClass.Name);
                    cmd.Parameters.AddWithValue("@num", newClass.Number);
                    cmd.Parameters.AddWithValue("@term", newClass.Term);
                    cmd.Parameters.AddWithValue("@hours", newClass.CreditHours);
                    cmd.Parameters.AddWithValue("@track", newClass.Track);

                    MySqlDataReader rdr = cmd.ExecuteReader();
                    rdr.Close();
                    conn.Close();
                }
                // Updates the List View Afterwards
                lvClasses.SelectedItems[0].Tag = newClass;
                lvClasses.SelectedItems[0].Text = newClass.ToString();
                lvClasses.SelectedItems[0].ImageIndex = newClass.Track;
            }
            catch (Exception er)
            {
                MessageBox.Show("Update Failed, Please Make sure object already exists in List View before Updating.");
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            prntDialog.Show();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            // Creates a MobileDev object with data stored in the ListView object that the user double clicked.
            MobileDev newClass = (MobileDev)lvClasses.SelectedItems[0].Tag;
            try
            {
                // checks if a connection is open and then updates the database
                if (conn.State == ConnectionState.Open)
                {
                    string stm = "DELETE FROM classes WHERE CourseName = @name AND CourseNumber = @num";

                    MySqlCommand cmd = new MySqlCommand(stm, conn);
                    cmd.Parameters.AddWithValue("@name", newClass.Name);
                    cmd.Parameters.AddWithValue("@num", newClass.Number);

                    MySqlDataReader rdr = cmd.ExecuteReader();
                    rdr.Close();
                }
                else
                {
                    conn.Open();
                    string stm = "DELETE FROM classes WHERE CourseName = @name AND CourseNumber = @num";

                    MySqlCommand cmd = new MySqlCommand(stm, conn);
                    cmd.Parameters.AddWithValue("@name", newClass.Name);
                    cmd.Parameters.AddWithValue("@num", newClass.Number);

                    MySqlDataReader rdr = cmd.ExecuteReader();
                    rdr.Close();
                    conn.Close();
                }
                // Removes the List View Afterwards
                lvClasses.SelectedItems[0].Remove();
            }
            catch (Exception er)
            {
                MessageBox.Show($"Delete Failed!\r\n{er}");
            }
        }



    }
}
