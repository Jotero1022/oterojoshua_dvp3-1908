﻿namespace OteroJoshua_CE02
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.lblName = new System.Windows.Forms.Label();
            this.lblNumber = new System.Windows.Forms.Label();
            this.lblTerm = new System.Windows.Forms.Label();
            this.lblHours = new System.Windows.Forms.Label();
            this.lblTrack = new System.Windows.Forms.Label();
            this.cmbTrack = new System.Windows.Forms.ComboBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtNumber = new System.Windows.Forms.TextBox();
            this.txtTerm = new System.Windows.Forms.TextBox();
            this.numHours = new System.Windows.Forms.NumericUpDown();
            this.lvClasses = new System.Windows.Forms.ListView();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.imgIcons = new System.Windows.Forms.ImageList(this.components);
            this.prntDialog = new System.Windows.Forms.PrintPreviewDialog();
            this.btnDelete = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numHours)).BeginInit();
            this.SuspendLayout();
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(97, 202);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(74, 13);
            this.lblName.TabIndex = 0;
            this.lblName.Text = "Course Name:";
            // 
            // lblNumber
            // 
            this.lblNumber.AutoSize = true;
            this.lblNumber.Location = new System.Drawing.Point(88, 236);
            this.lblNumber.Name = "lblNumber";
            this.lblNumber.Size = new System.Drawing.Size(83, 13);
            this.lblNumber.TabIndex = 1;
            this.lblNumber.Text = "Course Number:\r\n";
            // 
            // lblTerm
            // 
            this.lblTerm.AutoSize = true;
            this.lblTerm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTerm.Location = new System.Drawing.Point(137, 270);
            this.lblTerm.Name = "lblTerm";
            this.lblTerm.Size = new System.Drawing.Size(34, 13);
            this.lblTerm.TabIndex = 2;
            this.lblTerm.Text = "Term:";
            // 
            // lblHours
            // 
            this.lblHours.AutoSize = true;
            this.lblHours.Location = new System.Drawing.Point(103, 304);
            this.lblHours.Name = "lblHours";
            this.lblHours.Size = new System.Drawing.Size(68, 13);
            this.lblHours.TabIndex = 3;
            this.lblHours.Text = "Credit Hours:";
            // 
            // lblTrack
            // 
            this.lblTrack.AutoSize = true;
            this.lblTrack.Location = new System.Drawing.Point(131, 338);
            this.lblTrack.Name = "lblTrack";
            this.lblTrack.Size = new System.Drawing.Size(38, 13);
            this.lblTrack.TabIndex = 4;
            this.lblTrack.Text = "Track:";
            // 
            // cmbTrack
            // 
            this.cmbTrack.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTrack.FormattingEnabled = true;
            this.cmbTrack.Items.AddRange(new object[] {
            "Central",
            "Mobile Dev"});
            this.cmbTrack.Location = new System.Drawing.Point(175, 333);
            this.cmbTrack.Name = "cmbTrack";
            this.cmbTrack.Size = new System.Drawing.Size(121, 21);
            this.cmbTrack.TabIndex = 5;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(177, 199);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(191, 20);
            this.txtName.TabIndex = 6;
            // 
            // txtNumber
            // 
            this.txtNumber.Location = new System.Drawing.Point(177, 233);
            this.txtNumber.Name = "txtNumber";
            this.txtNumber.Size = new System.Drawing.Size(100, 20);
            this.txtNumber.TabIndex = 7;
            // 
            // txtTerm
            // 
            this.txtTerm.Location = new System.Drawing.Point(175, 267);
            this.txtTerm.Name = "txtTerm";
            this.txtTerm.Size = new System.Drawing.Size(100, 20);
            this.txtTerm.TabIndex = 8;
            // 
            // numHours
            // 
            this.numHours.DecimalPlaces = 1;
            this.numHours.Location = new System.Drawing.Point(177, 302);
            this.numHours.Maximum = new decimal(new int[] {
            40,
            0,
            0,
            65536});
            this.numHours.Name = "numHours";
            this.numHours.ReadOnly = true;
            this.numHours.Size = new System.Drawing.Size(48, 20);
            this.numHours.TabIndex = 9;
            // 
            // lvClasses
            // 
            this.lvClasses.LargeImageList = this.imgIcons;
            this.lvClasses.Location = new System.Drawing.Point(91, 519);
            this.lvClasses.Name = "lvClasses";
            this.lvClasses.Size = new System.Drawing.Size(511, 383);
            this.lvClasses.SmallImageList = this.imgIcons;
            this.lvClasses.TabIndex = 10;
            this.lvClasses.UseCompatibleStateImageBehavior = false;
            this.lvClasses.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lvClasses_MouseDoubleClick);
            // 
            // btnPrint
            // 
            this.btnPrint.Location = new System.Drawing.Point(175, 397);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(96, 39);
            this.btnPrint.TabIndex = 11;
            this.btnPrint.Text = "Print";
            this.btnPrint.UseVisualStyleBackColor = true;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(396, 397);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(96, 39);
            this.btnUpdate.TabIndex = 12;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(175, 451);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(96, 39);
            this.btnAdd.TabIndex = 13;
            this.btnAdd.Text = "Add New";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // imgIcons
            // 
            this.imgIcons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgIcons.ImageStream")));
            this.imgIcons.TransparentColor = System.Drawing.Color.Transparent;
            this.imgIcons.Images.SetKeyName(0, "Home");
            this.imgIcons.Images.SetKeyName(1, "Mobile");
            // 
            // prntDialog
            // 
            this.prntDialog.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.prntDialog.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.prntDialog.ClientSize = new System.Drawing.Size(400, 300);
            this.prntDialog.Enabled = true;
            this.prntDialog.Icon = ((System.Drawing.Icon)(resources.GetObject("prntDialog.Icon")));
            this.prntDialog.Name = "prntDialog";
            this.prntDialog.Visible = false;
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(396, 451);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(96, 39);
            this.btnDelete.TabIndex = 14;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackgroundImage = global::OteroJoshua_CE02.Properties.Resources.iPhone7Image;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(671, 1061);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.lvClasses);
            this.Controls.Add(this.numHours);
            this.Controls.Add(this.txtTerm);
            this.Controls.Add(this.txtNumber);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.cmbTrack);
            this.Controls.Add(this.lblTrack);
            this.Controls.Add(this.lblHours);
            this.Controls.Add(this.lblTerm);
            this.Controls.Add(this.lblNumber);
            this.Controls.Add(this.lblName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.numHours)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblNumber;
        private System.Windows.Forms.Label lblTerm;
        private System.Windows.Forms.Label lblHours;
        private System.Windows.Forms.Label lblTrack;
        private System.Windows.Forms.ComboBox cmbTrack;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtNumber;
        private System.Windows.Forms.TextBox txtTerm;
        private System.Windows.Forms.NumericUpDown numHours;
        private System.Windows.Forms.ListView lvClasses;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.ImageList imgIcons;
        private System.Windows.Forms.PrintPreviewDialog prntDialog;
        private System.Windows.Forms.Button btnDelete;
    }
}

