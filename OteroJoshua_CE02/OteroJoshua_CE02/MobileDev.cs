﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OteroJoshua_CE02
{
    class MobileDev
    {
        public string Name;

        public string Number;

        public string Term;

        public decimal CreditHours;

        public int Track;

        public override string ToString()
        {
            if (Name.Length > 10)
            {
                return $"{Name.Substring(0, 10)}...";
            }
            else
            {
                return $"{Name}";
            }
        }
    }
}
